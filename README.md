# jupyter_cis575
CIS575 Data Analysis with Python / Jupyter

Team project for Data Analytics Course.

Using Python / Jupyter to do initial dataset assembly and grooming before moving it into SAS Enterprise Miner for Analysis.

Note: All this could be done in SAS Enterprise Miner, but using Python / Jupyter Notebook because:
1) Lack of familiarity with SAS Enterprise Miner is slowing me down during this stage.
2) I've been looking for an excuse to use a Jupyter Notebook and pandas module, and this fits perfectly.
